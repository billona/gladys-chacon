const Promise = require('bluebird');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const fs = require('fs');

module.exports = function init() {

    fs.chmodSync('/home/pi/gladys/api/hooks/chacon/lib/wiringPi/build', '555');
    fs.chmodSync('/home/pi/gladys/api/hooks/chacon/lib/init.sh', '555');
    return exec('/home/pi/gladys/api/hooks/chacon/lib/init.sh');
    
};