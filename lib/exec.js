const Promise = require('bluebird');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

module.exports = function (params) {

    var command = params.deviceType.identifier;
    var name = params.deviceType.name;
    var room = params.deviceType.room;
    var value = params.state.value;

    if ((typeof command === 'undefined') || command === null || command === '') {
        return Promise.reject(new Error('gladys-chacon: This device does not have any command to execute.'));
    } else {

        if (value === 1) {
            sails.log.debug('gladys-chacon: Switching on device ' + name + ' in room ' + room);
            sails.log.debug('gladys-chacon: Executing command ' + command + ' on');
            return exec("/home/pi/gladys/api/hooks/chacon/lib/chacon_send 4 " + command + " on");       // Number 4 is for GPIO pin
        } else {                                                                                        // where 433 MHz emitter is connected
            sails.log.debug('gladys-chacon: Switching off device ' + name + ' in room ' + room);        // (according to wiringPi)
            sails.log.debug('gladys-chacon: Executing command ' + command + ' off');                    
            return exec("/home/pi/gladys/api/hooks/chacon/lib/chacon_send 4 " + command + " off");      
        }
        
    }
}
