# Gladys-chacon

Gladys-Chacon is a module allowing to control Chacon DiO outlets with Gladys.

## Installation:

As those outlets have a learning phase any they can respond to any code.
Therefore you just have to install the module as described here, create your device and choose the code you want the outlet to respond !

* install the module :
    * Modules > Advanced
    * Nom : Chacon
    * Version : 0.2.0
    * URL git : https://gitlab.com/billona/gladys-chacon.git
    * Slug : chacon

* restart Gladys.

* Create a device like this one: 
    * Name = Device name || identifier = <controler_code> <outlet_code> || protocol = Chacon || Service = chacon ||
    * <controler_code> : Emitter's identification code. This is a code on 26 bits. Any code can be put for the device to respond to this code.
    * <outlet_code> : Outlet's identification code. If an outlet has just been put in place on the sector, it will take that code as identifier and will memorize it.

* go to edit and create a devicetype like that:
    * type: binary()
    * max: 1
    * min: 0

Now you can control your Chacon devices.

I hope this module will help you by making control of this technology easier ;)