module.exports = function (sails) {
    return {
        exec: require('./lib/exec'),
        install: require('./lib/install')
    };
};